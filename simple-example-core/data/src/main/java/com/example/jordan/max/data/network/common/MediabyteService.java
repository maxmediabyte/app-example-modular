package com.example.jordan.max.data.network.common;

import com.example.jordan.max.data.network.ServerConstants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public class MediabyteService implements IMediabyteService,Interceptor{

    private static MediabyteService INSTANCE;
    private Retrofit retrofit;
    private MediabyteService()
    {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if(ServerConstants.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }
        builder.addInterceptor(this);
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ServerConstants.BASE_URL)
                .client(builder.build())
                .build();
    }

    public synchronized static MediabyteService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MediabyteService();
        }
        return INSTANCE;
    }

    @Override
    public <T> T provideApi(Class<T> clazz) {
        return retrofit.create(clazz);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("application-type", "REST")
                .build();

        return chain.proceed(request);
    }
}
