package com.example.jordan.max.data.interactors;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.entity.Item;
import com.example.jordan.max.data.helpers.CallbackService;
import com.example.jordan.max.data.repository.items.ItemsDataSource;
import com.example.usecase.DetailItem;
import com.example.usecase.GetListItems;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public class ItemsInteractor {
    private  static  ItemsInteractor mItemsInteractor;
    private ItemsDataSource mRemoteItemsDataSource;
    private ItemsInteractor(ItemsDataSource mRemoteItemsDataSource) {
        this.mRemoteItemsDataSource = mRemoteItemsDataSource;
    }
    public static synchronized ItemsInteractor getInstance(@NonNull ItemsDataSource mRemoteItemsDataSource)
    {
        if(mItemsInteractor == null){
            mItemsInteractor=new ItemsInteractor(mRemoteItemsDataSource);
        }
        return mItemsInteractor;
    }

    public void getListItems(CallbackService.SuccessCallback<GetListItems.Response> successCallback,
                             CallbackService.ErrorCallback errorCallback)
    {
        mRemoteItemsDataSource.getListItems(data -> {
            successCallback.onSuccess(data);
        },exception -> {
            errorCallback.onError(exception);
        });
    }

    public void getDetailItem(String idItem ,CallbackService.SuccessCallback<DetailItem.Response> successCallback,
                             CallbackService.ErrorCallback errorCallback)
    {
        mRemoteItemsDataSource.getDetailItem(idItem,data -> {
            successCallback.onSuccess(data);
        },exception -> {
            errorCallback.onError(exception);
        });
    }
}
