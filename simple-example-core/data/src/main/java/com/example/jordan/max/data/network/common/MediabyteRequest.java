package com.example.jordan.max.data.network.common;

import com.example.jordan.max.data.helpers.CallbackService;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public interface MediabyteRequest<T> {
    void enqueue(CallbackService.SuccessCallback<T> successCallback,
                  CallbackService.ErrorCallback errorCallback);
    void cancel();
}
