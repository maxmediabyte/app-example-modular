package com.example.jordan.max.data.network;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public class ServerConstants {
    public static final boolean DEBUG = true;
    public static final String BASE_URL = "http://www.revista.sodimac.mccann.mediabytelab.com/internas/gateway-testing/index.php/api/";
    public static final String API_ITEM_LIST = "list-items";
    public static final String API_ITEM_DETAIL= "detail-item";
}
