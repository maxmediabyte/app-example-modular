package com.example.jordan.max.data.helpers;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public interface CallbackService {
    interface SuccessCallback<T>{
        void onSuccess(T response);
    }
    interface ErrorCallback{
        void onError(Exception exception);
    }
}
