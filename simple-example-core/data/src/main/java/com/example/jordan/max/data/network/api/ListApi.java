package com.example.jordan.max.data.network.api;

import com.example.jordan.max.data.network.ServerConstants;
import com.example.usecase.DetailItem;
import com.example.usecase.GetListItems;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public interface ListApi {
    @POST(ServerConstants.API_ITEM_LIST)
    Call<GetListItems.Response> getListItems();
    @POST(ServerConstants.API_ITEM_DETAIL)
    Call<DetailItem.Response> getDetailItem(@Body DetailItem.Request DetailItemRequest);
}
