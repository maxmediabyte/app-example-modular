package com.example.jordan.max.data;

import com.example.jordan.max.data.interactors.ItemsInteractor;
import com.example.jordan.max.data.network.common.MediabyteService;
import com.example.jordan.max.data.repository.items.RemoteItemsDataSource;

/**
 * Created by Mediabyte on 26/06/2017.
 */

public class Injection {
    public static ItemsInteractor providerItemsInteractor() {
        return ItemsInteractor.getInstance(
                RemoteItemsDataSource.getInstance(MediabyteService.getInstance())
                );
    }
}
