package com.example.jordan.max.data.repository.items;

import com.example.jordan.max.data.helpers.CallbackService;
import com.example.usecase.DetailItem;
import com.example.usecase.GetListItems;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public interface ItemsDataSource {
    void getListItems(CallbackService.SuccessCallback<GetListItems.Response> successCallback,
                      CallbackService.ErrorCallback errorCallback);
    void getDetailItem(String idItem,CallbackService.SuccessCallback<DetailItem.Response> successCallback,
                      CallbackService.ErrorCallback errorCallback);
}
