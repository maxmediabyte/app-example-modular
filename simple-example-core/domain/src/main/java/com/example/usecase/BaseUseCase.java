package com.example.usecase;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public class BaseUseCase {
    private String message;
    private String status;

    public BaseUseCase() {
    }

    public BaseUseCase(String message, String status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
