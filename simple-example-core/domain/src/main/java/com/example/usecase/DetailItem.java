package com.example.usecase;

import com.example.entity.Item;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Mediabyte on 26/06/2017.
 */

public class DetailItem {

    public static class Request {
        @SerializedName("idItem")
        private String idItem;

        public Request(String idItem) {
            this.idItem = idItem;
        }
    }
    public static class Response  extends BaseUseCase{
        private Item item;
        private String idItem;

        public Response(Item mItem) {
            item = mItem;
        }

        public Item getItems() {
            return item;
        }

        public void setItems(Item item) {
            this.item = item;
        }

        public String getIdItem() {
            return idItem;
        }

        public void setIdItem(String idItem) {
            this.idItem = idItem;
        }
    }
}
