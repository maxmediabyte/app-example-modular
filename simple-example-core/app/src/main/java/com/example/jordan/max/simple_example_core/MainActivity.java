package com.example.jordan.max.simple_example_core;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.entity.Item;
import com.example.jordan.max.data.Injection;
import com.example.jordan.max.data.interactors.ItemsInteractor;
import com.example.jordan.max.data.network.common.MediabyteService;
import com.example.jordan.max.data.repository.items.RemoteItemsDataSource;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*INJECTOR OF THE INTERACTORS*/
        ItemsInteractor itemsInteractor = Injection.providerItemsInteractor();

        itemsInteractor.getListItems(data -> {
            for (Item item : data.getListItems())
                Log.d("data => ",item.getName());
        },exception -> {
            Log.d("error =>" ,exception.getMessage().toString());
        });

        itemsInteractor.getDetailItem("1",data -> {
            Log.d("Item",data.getItems().toString());
            Log.d("id Item",data.getIdItem());
        },exception -> {
            Log.d("error =>" ,exception.getMessage().toString());
        });
    }
}
