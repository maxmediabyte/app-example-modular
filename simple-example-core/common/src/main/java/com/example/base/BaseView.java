package com.example.base;

/**
 * Created by Mediabyte on 07/06/2017.
 */

public interface BaseView {
    void startLoading();
    void stopLoading();
}
