package com.example.jordan.max.simple_example_app.presentation.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.ViewTarget;
import com.example.entity.Item;
import com.example.jordan.max.simple_example_app.R;
import com.example.jordan.max.simple_example_app.common.BaseAdapter;
import com.example.jordan.max.simple_example_app.presentation.detail.DetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public class ItemsAdapter extends BaseAdapter<Item> implements View.OnClickListener{

    private View.OnClickListener listener;
    public ItemsAdapter(Context context,ArrayList<Item> items) {
        super(context);
        mList=items;
    }
    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false);
        view.setOnClickListener(this);
        return new ItemsAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder != null) {

            ((ItemViewHolder) holder).mTitleItemTextView.setText(getItem(position).getName());
            ((ItemViewHolder) holder).mStatusItemTextView.setText(
                    (getItem(position).getStatus().equalsIgnoreCase("1"))?"Activo":"Inactivo"
            );
            Glide.with(mContext).load(getItem(position).geturlImg()).into(((ItemViewHolder) holder).mImageItem);
        }
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitleItem)
        TextView mTitleItemTextView;
        @BindView(R.id.imgItem)
        ImageView mImageItem;
        @BindView(R.id.txtStatus)
        TextView mStatusItemTextView;
        public ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
