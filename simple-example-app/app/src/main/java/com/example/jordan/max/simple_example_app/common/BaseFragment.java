package com.example.jordan.max.simple_example_app.common;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.base.BaseView;


/**
 * Created by Mediabyte on 07/06/2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView {
    protected Context mContext;

    protected abstract int getLayoutView();
    protected abstract void setupView(Bundle savedInstanceState);

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutView(), container, false);
        if (view != null) {
            return view;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void startLoading() {
        ((BaseActivity) mContext).startLoading();
    }

    @Override
    public void stopLoading() {
        ((BaseActivity) mContext).stopLoading();
    }
}
