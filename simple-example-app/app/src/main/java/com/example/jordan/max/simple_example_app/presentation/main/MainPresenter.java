package com.example.jordan.max.simple_example_app.presentation.main;

import android.util.Log;

import com.example.entity.Item;
import com.example.jordan.max.data.interactors.ItemsInteractor;
import com.example.jordan.max.simple_example_app.common.BasePresenter;

import java.util.ArrayList;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public class MainPresenter extends BasePresenter implements MainContract.Presenter{
    private MainContract.MainView mView;
    private ItemsInteractor itemsInteractor;
    private static MainPresenter mMainPresenter;
    public MainPresenter(MainContract.MainView view,
                         ItemsInteractor itemsItenteractor) {
        this.mView = view;
        itemsInteractor = itemsItenteractor;
    }
    public static MainPresenter getInstance(MainContract.MainView mView,
                                            ItemsInteractor mItemsInteractor)
    {
        mMainPresenter = new MainPresenter(
                mView,
                mItemsInteractor
        );
        return mMainPresenter;
    }

    @Override
    protected void start() {

    }

    @Override
    public void getListItems() {

        itemsInteractor.getListItems(data -> {
            mView.onCompleteListItems((ArrayList<Item>) data.getListItems());
        },exception -> {
            Log.d("error =>" ,exception.getMessage().toString());
        });
    }
}

