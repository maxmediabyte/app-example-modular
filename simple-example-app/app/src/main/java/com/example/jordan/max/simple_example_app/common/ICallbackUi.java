package com.example.jordan.max.simple_example_app.common;

/**
 * Created by Mediabyte on 22/06/2017.
 */

public interface ICallbackUi<T> {
    void onSuccess(T response);
    void onError(Exception exception);
}
