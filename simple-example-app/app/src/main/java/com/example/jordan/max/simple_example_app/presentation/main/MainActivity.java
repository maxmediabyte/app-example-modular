package com.example.jordan.max.simple_example_app.presentation.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.example.entity.Item;
import com.example.jordan.max.data.Injection;
import com.example.jordan.max.data.interactors.ItemsInteractor;
import com.example.jordan.max.data.network.common.MediabyteService;
import com.example.jordan.max.data.repository.items.RemoteItemsDataSource;
import com.example.jordan.max.simple_example_app.R;
import com.example.jordan.max.simple_example_app.common.BaseActivity;
import com.example.jordan.max.simple_example_app.presentation.detail.DetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnItemClick;


/**
 * Created by Mediabyte on 17/06/2017.
 */

public class MainActivity extends BaseActivity implements MainContract.MainView{
    private MainPresenter mMainPresenter;
    @BindView(R.id.recyclerListItem)
    RecyclerView recyclerListItem;
    @Override
    protected int getLayoutView() {
        return R.layout.activity_main;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        mMainPresenter.getListItems();

    }

    @Override
    protected void getPresenter() {
        mMainPresenter = MainPresenter.getInstance(this,
                Injection.providerItemsInteractor()
        );
    }

    @Override
    public void onCompleteListItems(ArrayList<Item> listItems) {
        ItemsAdapter itemsAdapter= new ItemsAdapter(this,listItems);
        recyclerListItem.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerListItem.setAdapter(itemsAdapter);
        itemsAdapter.setOnClickListener(v -> {
            int position = recyclerListItem.getChildAdapterPosition(v);
            String id =itemsAdapter.getItem(position).getId();
            Intent intent = new Intent(getBaseContext(), DetailActivity.class);
            intent.putExtra("idItem", id);
            startActivity(intent);
        });
    }
}

