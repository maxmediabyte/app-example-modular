package com.example.jordan.max.simple_example_app.presentation.detail;


import com.example.entity.Item;

import java.util.ArrayList;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public interface DetailContract {
    interface MainView{
        void onCompleteItem(Item mItem,String mIdItem);
    }
    interface Presenter {
        void getDetailItem(String id);
    }
}
