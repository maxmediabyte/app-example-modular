package com.example.jordan.max.simple_example_app.presentation.detail;

import android.util.Log;

import com.example.entity.Item;
import com.example.jordan.max.data.interactors.ItemsInteractor;
import com.example.jordan.max.simple_example_app.common.BasePresenter;

import java.util.ArrayList;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public class DetailPresenter extends BasePresenter implements DetailContract.Presenter{
    private DetailContract.MainView mView;
    private ItemsInteractor itemsInteractor;
    private static DetailPresenter mMainPresenter;
    public DetailPresenter(DetailContract.MainView view,
                           ItemsInteractor itemsItenteractor) {
        this.mView = view;
        itemsInteractor = itemsItenteractor;
    }
    public static DetailPresenter getInstance(DetailContract.MainView mView,
                                              ItemsInteractor mItemsInteractor)
    {
        mMainPresenter = new DetailPresenter(
                mView,
                mItemsInteractor);
        return mMainPresenter;
    }

    @Override
    protected void start() {

    }

    @Override
    public void getDetailItem(String idItem) {

        itemsInteractor.getDetailItem(idItem,data -> {
            mView.onCompleteItem(data.getItems(),data.getIdItem());
        },exception -> {
            Log.d("error =>" ,exception.getMessage().toString());
        });
    }
}

