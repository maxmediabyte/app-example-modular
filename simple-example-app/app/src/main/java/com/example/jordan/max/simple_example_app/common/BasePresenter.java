package com.example.jordan.max.simple_example_app.common;

/**
 * Created by Mediabyte on 08/06/2017.
 */

public abstract class BasePresenter {
    protected abstract void start();
}
