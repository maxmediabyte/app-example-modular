package com.example.jordan.max.simple_example_app.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected ArrayList<T> mList;
    protected Context mContext;

    public interface OnItemClickListener {
        void onItemClick(Object object, int position);
    }

    protected OnItemClickListener mItemClickListener;

    public BaseAdapter(Context context) {
        super();
        mContext = context;
        mList = new ArrayList<>();
    }

    public void addItems(@NonNull ArrayList<T> list){

        mList.addAll(list);
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        if (mList != null && position >= 0 && position < mList.size()) {
            return mList.get(position);
        }
        return null;
    }

    public ArrayList<T> getList() {
        return mList;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

}
