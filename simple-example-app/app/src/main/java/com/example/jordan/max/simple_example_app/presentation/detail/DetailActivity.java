package com.example.jordan.max.simple_example_app.presentation.detail;


import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.entity.Item;
import com.example.jordan.max.data.Injection;
import com.example.jordan.max.data.interactors.ItemsInteractor;
import com.example.jordan.max.data.network.common.MediabyteService;
import com.example.jordan.max.data.repository.items.RemoteItemsDataSource;
import com.example.jordan.max.simple_example_app.R;
import com.example.jordan.max.simple_example_app.common.BaseActivity;

import butterknife.BindView;


/**
 * Created by Mediabyte on 17/06/2017.
 */

public class DetailActivity extends BaseActivity implements DetailContract.MainView{
    @BindView(R.id.mTxtIdSelected)
    TextView mTxtIdSelected;
    @BindView(R.id.mTxtNameItem)
    TextView mTxtNameItem;
    @BindView(R.id.mImgItem)
    ImageView mImgItem;

    private DetailPresenter mDetailPresenter;
    @Override
    protected int getLayoutView() {
        return R.layout.detail_main;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        mDetailPresenter.getDetailItem(getIntent().getStringExtra("idItem"));
    }

    @Override
    protected void getPresenter() {
        mDetailPresenter = DetailPresenter.getInstance(this,
                ItemsInteractor.getInstance(
                        RemoteItemsDataSource.getInstance(MediabyteService.getInstance())
                )
        );
    }



    @Override
    public void onCompleteItem(Item mItem,String mIdItem) {
        mTxtIdSelected.setText(mIdItem);
        mTxtNameItem.setText(mItem.getName());
        Glide.with(getBaseContext()).load(mItem.geturlImg()).into(mImgItem);
    }
}
