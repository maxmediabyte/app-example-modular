package com.example.jordan.max.simple_example_app.presentation.main;


import com.example.entity.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public interface MainContract {
    interface MainView{
        void onCompleteListItems(ArrayList<Item> listItems);
    }
    interface Presenter {
        void getListItems();
    }
}
