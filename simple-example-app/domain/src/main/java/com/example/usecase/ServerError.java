package com.example.usecase;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public class ServerError {
    public static final int UNKNOWN = -999;
    public static final int CANCELLED = -1000;

    @SerializedName("status")
    private int mStatus;

    @SerializedName("message")
    private String mMessage;

    public ServerError(int status, String message) {
        mStatus = status;
        mMessage = message;
    }

    public int getStatus() {
        return mStatus;
    }

    public String getMessage() {
        return mMessage;
    }

    public Exception createException(Throwable cause) {
        if (cause != null) {
            return new Exception(mMessage, cause);
        }
        return new Exception(mMessage);
    }
}
