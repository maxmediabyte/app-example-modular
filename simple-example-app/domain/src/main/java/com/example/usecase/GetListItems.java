package com.example.usecase;

import com.example.entity.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mediabyte on 20/06/2017.
 */

public class GetListItems extends BaseUseCase{
    private List<Item> listItems;

    public GetListItems() {
        listItems = new ArrayList<>();
    }

    public GetListItems(List<Item> listItems) {
        listItems = listItems;
    }

    public List<Item> getListItems() {
        return listItems;
    }

    public void setListItems(List<Item> listItems) {
        listItems = listItems;
    }
}
