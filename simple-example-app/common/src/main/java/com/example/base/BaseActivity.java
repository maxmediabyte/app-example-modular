package com.example.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.example.jordan.max.common.R;

/**
 * Created by Mediabyte on 07/06/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView{
    private ProgressDialog mProgressLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutView());
        setupView(savedInstanceState);
        getPresenter();
    }
    protected abstract int getLayoutView();
    protected abstract void setupView(Bundle savedInstanceState);
    protected abstract void getPresenter();

    @Override
    public void startLoading() {
        if(mProgressLoading!=null){
            mProgressLoading=new ProgressDialog(this);
            mProgressLoading.setMessage(getString(R.string.message_loading));
            mProgressLoading.setIndeterminateDrawable(ContextCompat.getDrawable(this,R.drawable.drawable_progress));
            mProgressLoading.setCancelable(false);
            mProgressLoading.show();
        }
    }

    @Override
    public void stopLoading() {
        if(mProgressLoading!=null){
            mProgressLoading.dismiss();
        }
    }
}
