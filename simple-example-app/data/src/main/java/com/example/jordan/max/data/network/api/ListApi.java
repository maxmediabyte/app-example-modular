package com.example.jordan.max.data.network.api;

import com.example.jordan.max.data.network.ServerConstants;
import com.example.usecase.GetListItems;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public interface ListApi {
    @GET(ServerConstants.API_LIST)
    Call<GetListItems> getListItems() ;
}
