package com.example.jordan.max.data.interactors;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.entity.Item;
import com.example.jordan.max.data.repository.items.ItemsDataSource;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public class ItemsInteractor {
    private  static  ItemsInteractor INSTANCE;
    private ItemsDataSource mRemoteItemsDataSource;
    private ItemsInteractor(ItemsDataSource mRemoteItemsDataSource) {
        this.mRemoteItemsDataSource = mRemoteItemsDataSource;
    }
    public static ItemsInteractor getInstance(@NonNull ItemsDataSource mRemoteItemsDataSource)
    {
        if(INSTANCE == null){
            INSTANCE=new ItemsInteractor(mRemoteItemsDataSource);
        }
        return INSTANCE;
    }

    public void getListItems()
    {
        mRemoteItemsDataSource.getListItems(data -> {
            for (Item item : data.getListItems())
                Log.d("data => ",item.getName());
        },exception -> {
            Log.d("error =>" ,exception.getMessage().toString());
        });
    }
}
