package com.example.jordan.max.data.network.common;

import com.example.jordan.max.data.helpers.CallbackService;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public class ServerCallback<T> implements Callback<T>{
    private CallbackService.SuccessCallback<T> mSuccessCallback;
    private CallbackService.ErrorCallback mErrorCallback;
    public ServerCallback(CallbackService.SuccessCallback<T> mSuccessCallback,
                          CallbackService.ErrorCallback mErrorCallback) {
        this.mSuccessCallback = mSuccessCallback;
        this.mErrorCallback = mErrorCallback;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            mSuccessCallback.onSuccess(response.body());
        } else {
            ServerError serverError = null;
            if (response.errorBody() != null) {
                Gson gson = new Gson();
                try {
                    serverError = gson.fromJson(response.errorBody().string(), ServerError.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (serverError == null) {
                serverError = new ServerError(ServerError.UNKNOWN, "Server Failure");
            }
            mErrorCallback.onError(serverError.createException(null));
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        ServerError serverError;
        if (call.isCanceled()) {
            serverError = new ServerError(ServerError.CANCELLED, "Request cancelled by client");
        } else {
            serverError = new ServerError(ServerError.UNKNOWN, "Something went wrong :(");
        }
        mErrorCallback.onError(serverError.createException(t));
    }
}
