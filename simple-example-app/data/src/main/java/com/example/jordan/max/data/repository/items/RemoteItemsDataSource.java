package com.example.jordan.max.data.repository.items;

import com.example.jordan.max.data.helpers.CallbackService;
import com.example.jordan.max.data.network.common.IMediabyteService;
import com.example.jordan.max.data.network.common.ServerRequest;
import com.example.jordan.max.data.network.api.ListApi;
import com.example.usecase.GetListItems;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public class RemoteItemsDataSource implements ItemsDataSource {
    private static RemoteItemsDataSource INSTANCE;
    private ListApi ListApi;

    public RemoteItemsDataSource(IMediabyteService mediabyteService) {
        ListApi = mediabyteService.provideApi(ListApi.class);
    }

    public static RemoteItemsDataSource getInstance(IMediabyteService mediabyteService){
        if(INSTANCE==null){
            INSTANCE = new RemoteItemsDataSource(mediabyteService);
        }
        return INSTANCE;
    }


    @Override
    public void getListItems(CallbackService.SuccessCallback<GetListItems> successCallback,
                             CallbackService.ErrorCallback errorCallback) {
        ServerRequest<GetListItems> request = new ServerRequest<>(ListApi.getListItems());
        request.enqueue(successCallback, errorCallback);
    }
}
