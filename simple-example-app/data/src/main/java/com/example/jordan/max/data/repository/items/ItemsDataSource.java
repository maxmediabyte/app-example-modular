package com.example.jordan.max.data.repository.items;

import com.example.jordan.max.data.helpers.CallbackService;
import com.example.usecase.GetListItems;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public interface ItemsDataSource {
    void getListItems(CallbackService.SuccessCallback<GetListItems> successCallback,
                      CallbackService.ErrorCallback errorCallback);
}
