package com.example.jordan.max.data.network.common;

import com.example.jordan.max.data.helpers.CallbackService;

import retrofit2.Call;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public class ServerRequest<T> implements MediabyteRequest<T> {
    private Call mCall;
    public ServerRequest(Call<T> call) {
        mCall = call;
    }

    @Override
    public void enqueue(CallbackService.SuccessCallback successCallback,
                         CallbackService.ErrorCallback errorCallback) {
        mCall.enqueue(new ServerCallback<T>(successCallback,errorCallback));
    }

    @Override
    public void cancel() {
        mCall.cancel();
    }
}
