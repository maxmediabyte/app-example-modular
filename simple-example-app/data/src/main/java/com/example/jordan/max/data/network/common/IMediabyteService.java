package com.example.jordan.max.data.network.common;

/**
 * Created by Mediabyte on 12/06/2017.
 */

public interface IMediabyteService {
    <T> T provideApi(final Class<T> clazz);
}
